#pragma once
#include <cassert>
#include "common.h"
#include "measurement.h"
#include "state.h"

class PoseGraph{

 public:
  PoseGraph() = default ;
  PoseGraph(unsigned int first_id, unsigned int last_id, unsigned int max_edge_one_node);
  void AddState(const State& state);
  void AddMeasurement(const Measurement& measurement);
  void PrintGraph(const std::string &name_window, unsigned int img_dim=900,
                  int line_thickness=1, int state_radius=7) const;
  void PrintGraph(const cv::Mat& image, char color, int offset) const;

 private:
  std::vector<int> map_id_vector_;
  unsigned int first_id_;
  unsigned int number_of_state_;
  unsigned int max_edge_one_node_;
  std::vector<State> states_;
  std::vector<Measurement> measurements_;
  inline const State& GetState(unsigned int id) const { 
    assert (this->map_id_vector_[id - this->first_id_] != -1);
    return this->states_.at(this->map_id_vector_[id - this->first_id_]);
  };
  void PrintMeasurement(const cv::Mat& image, const Measurement& measurement,
                        cv::Point& img_center, int scaling, int line_thickness ) const;

  friend class PGOSolver;
};