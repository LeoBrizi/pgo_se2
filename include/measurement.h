#pragma once
#include "common.h"

class Measurement{

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  Measurement(unsigned int id_from, unsigned int id_to, float z_x, float z_y,
              float z_theta, float omega_x_x, float omega_x_y, float omega_x_theta,
              float omega_y_y, float omega_y_theta, float omega_theta_theta);
  Measurement(unsigned int id_from, unsigned int id_to, const Eigen::Vector3f& observation,
              const Eigen::Matrix3f& information_matrix);
  Measurement(unsigned int id_from, unsigned int id_to, const Eigen::Matrix3f& observation);
  inline const Link& GetLink() const {return this->id_;};
  inline const Eigen::Matrix3f GetObservation() const {return this->observation_;};
  inline const Vector6f BoxMinus(const Eigen::Matrix3f& prediction) const {
    Vector6f error = Flatten(prediction) - Flatten(this->observation_);
    return error;
  };

 private:
  Link id_;
  Eigen::Matrix3f observation_;
  Eigen::Matrix3f information_matrix_;
};