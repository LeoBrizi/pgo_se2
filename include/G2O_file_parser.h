#pragma once
#include <fstream>
#include <sstream>
#include <string>
#include <climits>
#include <algorithm>
#include "pose_graph.h"

class G2OFileParser{
	
 public:
  G2OFileParser (const std::string &file_name);
  PoseGraph& GetPoseGraph();

 private:
  std::string file_name_;
  PoseGraph pose_graph_;
};