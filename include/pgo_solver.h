#pragma once
#include "common.h"
#include "pose_graph.h"

class PGOSolver{

 public:
  PGOSolver(PoseGraph *pose_graph, float kernel_threshold=1, int min_num_inliers=0, float damping=1);
  bool GoAndDoYourFunnyWork(bool ignore_outliers=false, bool print_h_mat=false);
  inline void SetKernelThreshold(float kernel_thereshold) {this->kernel_threshold_ = kernel_thereshold;};
  inline float GetKernelThreshold() {return this->kernel_threshold_;};
  inline void SetMinNumInliers(int min_num_inliers) {this->min_num_inliers_ = min_num_inliers;};
  inline int GetMinNumInliers() {return this->min_num_inliers_;};
  inline void PrintStat() {
    std::cout << "chi_inliers:        " << this->chi_inliers_ << "\n";
    std::cout << "chi_outliers:       " << this->chi_outliers_ << "\n";
    std::cout << "num_inliers:        " << this->num_inliers_ << "\n";
    std::cout << "optimization_time   " << this->optimization_time_ << "s\n";
    return;
  };

 private:
  PoseGraph *graph_;
  float kernel_threshold_;
  int min_num_inliers_;
  float damping_;
  float chi_inliers_;
  float chi_outliers_;
  int num_inliers_;
  float optimization_time_;
  void ErrorAndJacobian(const Measurement& measurement, Vector6f& error, Matrix6_3f& jacobian_d_xj) const;

};