#pragma once
#include "common.h"
#include "measurement.h"

class State{

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  State(unsigned int id, float x, float y, float theta);
  State(unsigned int id, const Eigen::Vector3f& pose);
  State(unsigned int id, const Eigen::Matrix3f& pose);
  inline const unsigned int& GetID() const {return this->id_;};
  inline const Eigen::Matrix3f& GetPose() const {return this->pose_;};
  inline const float& GetTheta() const {return this->theta_;};
  inline Eigen::Matrix2f GetOrientation() const {return this->pose_.block<2,2>(0, 0);};
  inline Eigen::Vector2f GetTranslation() const {return this->pose_.block<2,1>(0, 2);};
  const cv::Point GetGraphicalRepresentation() const;
  inline void BoxPlus(const Eigen::Vector3f& perturbation) {
    Eigen::Matrix3f new_pose = v2t(perturbation) * this->pose_;
    this->pose_ = new_pose;
    return;
  };
  inline const Eigen::Matrix3f Predict(const State& to) const {
    Eigen::Matrix3f h = InvHomogeneous(this->pose_) * to.pose_;
    return h;
  };

 private:
  unsigned int id_;
  Eigen::Matrix3f pose_;
  float theta_;
};