#pragma once
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/Geometry>
#include <Eigen/Cholesky>
#include <Eigen/StdVector>
#include <iostream>
#include <unistd.h>
#include "opencv2/opencv.hpp"
#include <math.h>
#include <algorithm> 
#include <sys/time.h>
#include <chrono>

#define OPENCV_KEY_SPACE 32
#define OPENCV_KEY_ESCAPE 27

struct link {
  unsigned int from;
  unsigned int to;
};

typedef struct link Link;
typedef Eigen::Matrix<float, 2, 3> Matrix2_3f;
typedef Eigen::Matrix<float, 6, 3> Matrix6_3f;
typedef Eigen::Matrix<float, 6, 1> Vector6f;
typedef Eigen::Matrix<float, 4, 1> Vector4f;
typedef Eigen::SparseMatrix<double> SparseMat;

inline double GetTime() {
  struct timeval tv;
  gettimeofday(&tv, 0);
  return 1e3 * tv.tv_sec + tv.tv_usec * 1e-3;
}

template<typename T, typename Char, typename Traits>
inline std::basic_istream<Char, Traits>& skip(std::basic_istream<Char, Traits>& stream) {
    T unused;
    return stream >> unused;
}

inline const Eigen::Matrix2f R(float theta) {
  float c = cos(theta);
  float s = sin(theta);
  Eigen::Matrix2f R;
  R << c,  -s,
       s,   c;
  return R;
}

inline const Eigen::Matrix2f RPrime(float theta) {
  float c = cos(theta);
  float s = sin(theta);
  Eigen::Matrix2f R;
  R << -s,  -c,
        c,  -s;
  return R;
}

inline const Eigen::Vector3f t2v(const Eigen::Matrix3f& t) {
  Eigen::Vector3f v;
  v.block<2,1>(0, 0) = t.block<2,1>(0, 2);
  v(2) = atan2(t(2, 1), t(1, 1));
  return v;
}

inline const Eigen::Matrix3f v2t(const Eigen::Vector3f& v) {
  Eigen::Matrix3f t;
  t.setIdentity();
  t.block<2,2>(0, 0) = R(v(2));
  t.block<2,1>(0, 2) = Eigen::Vector2f(v(0), v(1));
  return t;
}

inline const Eigen::Matrix3f InvHomogeneous(const Eigen::Matrix3f& h) {
  Eigen::Matrix3f inverse;
  Eigen::Matrix2f R = h.block<2,2>(0, 0);
  Eigen::Vector2f t = h.block<2,1>(0, 2);
  inverse.setIdentity();
  inverse.block<2,2>(0, 0) = R.transpose();
  inverse.block<2,1>(0, 2) = - R.transpose() * t;
  return inverse;
}

inline const Vector6f Flatten(const Eigen::Matrix3f& mat) {
  Vector6f vector;
  vector << mat(0, 0), mat(1, 0), mat(0, 1), mat(1, 1), mat(0, 2), mat(1, 2);
  return vector;
}

inline const Vector6f Flatten(const Matrix2_3f& mat) {
  Vector6f vector;
  vector << mat(0, 0), mat(1, 0), mat(0, 1), mat(1, 1), mat(0, 2), mat(1, 2);
  return vector;
}

inline const Vector4f Flatten(const Eigen::Matrix2f& mat) {
  Vector4f vector;
  vector << mat(0, 0), mat(1, 0), mat(0, 1), mat(1, 1);
  return vector;
}