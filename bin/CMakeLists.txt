link_libraries(${OpenCV_LIBS})

add_executable(show_graph show_graph.cpp 
  ../src/input_parser.cpp
  ../src/state.cpp
  ../src/measurement.cpp
  ../src/pose_graph.cpp
  ../src/G2O_file_parser.cpp
)
target_link_libraries(show_graph)

add_executable(pgo_se2_total pgo_se2_total.cpp 
  ../src/input_parser.cpp
  ../src/state.cpp
  ../src/measurement.cpp
  ../src/pose_graph.cpp
  ../src/G2O_file_parser.cpp
  ../src/pgo_solver.cpp
)
target_link_libraries(pgo_se2_total)
