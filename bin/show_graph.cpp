#include "input_parser.h"
#include "G2O_file_parser.h"
#include "pose_graph.h"

void PrintHelp() {
  std::cout << "Usage: show_graph [OPTIONS] [FILE]\n";
  std::cout << "  Print a single graph or the initial guess and the ground truth\n";
  std::cout << "  -f    single file g2o\n";
  std::cout << "  -g    ground truth\n";
  std::cout << "  -i    initial guess\n";
  std::cout << "  -h    display this help and exit\n";
  std::cout << "Exit status:\n";
  std::cout << "  0 if ok\n";
  std::cout << "  1 there was an error\n";
}

int main (int argc, char** argv) {
  InputParser input(argc, argv);

  if (input.CmdOptionExists("-h")) {
    PrintHelp();
    exit(0);
  }

  if (argc < 4 ) {
    if (input.CmdOptionExists("-f")) {
      const std::string &g20_file_name = input.GetCmdOption("-f");
      G2OFileParser file_parser(g20_file_name);
      const PoseGraph pgo_graph = file_parser.GetPoseGraph();
      pgo_graph.PrintGraph(g20_file_name);
      return 0;
    } else {
      PrintHelp();
      exit(1);
    }
  } else {
    if (input.CmdOptionExists("-g") && input.CmdOptionExists("-i")) {
      const std::string &g_t_g20_file_name = input.GetCmdOption("-g");
      G2OFileParser g_t_file_parser(g_t_g20_file_name);
      const PoseGraph g_t_pgo_graph = g_t_file_parser.GetPoseGraph();

      const std::string &i_g_g20_file_name = input.GetCmdOption("-i");
      G2OFileParser i_g_file_parser(i_g_g20_file_name);
      const PoseGraph i_g_pgo_graph = i_g_file_parser.GetPoseGraph();

      cv::Mat image(900, 900, CV_8UC3, cv::Scalar(255, 255, 255));
      g_t_pgo_graph.PrintGraph(image, 'b', 0);
      i_g_pgo_graph.PrintGraph(image, 'r', 3);
      cv::imshow("print ground truth(blue) and initial guess(red)", image);
      cv::waitKey(0);
      return 0;
    } else {
      PrintHelp();
      exit(1);
    }
  }
}
