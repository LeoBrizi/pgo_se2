#include "input_parser.h"
#include "G2O_file_parser.h"
#include "pose_graph.h"
#include "pgo_solver.h"
#include "common.h"

#define G_T_FILE_NAME "ground_truth_pgo2D.g2o"
#define I_G_FILE_NAME "initial_guess_pgo2D.g2o"

#define PRINTVERBOSE(verbose, string) ({    \
  if (verbose)                              \
    std::cout << string;                    \
})

void PrintHelp() {
  std::cout << "Usage: pgo_se2_total [OPTIONS] [FILE]\n";
  std::cout << "  Optimize the pose to pose graph\n";
  std::cout << "  -d    directory of dataset\n";
  std::cout << "  -v    verbose\n";
  std::cout << "  -ig   print also initial guess\n";
  std::cout << "  -kt   [float] kernel threshold for solver\n";
  std::cout << "  -in   [int] minimum number of inliers\n";
  std::cout << "  -da   [float] damping factor for solver\n";
  std::cout << "  -h    display this help and exit\n";
  std::cout << "Exit status:\n";
  std::cout << "  0 if ok\n";
  std::cout << "  1 there was an error\n";
}

int main (int argc, char** argv) {

  #ifdef SPARSE
    std::cout << "USING SPARSE MATRICES\n";
  #endif

  InputParser input(argc, argv);
  bool verbose = false;
  if (input.CmdOptionExists("-v"))
    verbose = true;
  bool print_initial_guess = false;
  if (input.CmdOptionExists("-ig"))
    print_initial_guess = true;
  if (input.CmdOptionExists("-h")) {
    PrintHelp();
    exit(0);
  }

  float kernel_threshold = 1;
  int min_num_inliers = 0;
  float damping = 1;
  if (input.CmdOptionExists("-kt"))
    kernel_threshold = input.GetFloat("-kt");
  if (input.CmdOptionExists("-in"))
    min_num_inliers = input.GetInt("-in");
  if (input.CmdOptionExists("-da"))
    damping = input.GetFloat("-da");

  if (input.CmdOptionExists("-d")) {
    const std::string &dir_name = input.GetCmdOption("-d");
    std::string ground_truth_file = dir_name + G_T_FILE_NAME; 
    std::string initial_guess_file = dir_name + I_G_FILE_NAME;
    PRINTVERBOSE(verbose, "loading ground truth graph...    ");
    G2OFileParser file_parser(ground_truth_file);
    PoseGraph g_t_graph = file_parser.GetPoseGraph();
    PRINTVERBOSE(verbose, "DONE\n");
    PRINTVERBOSE(verbose, "loading initial guess graph...   ");
    file_parser = G2OFileParser(initial_guess_file);
    PoseGraph i_g_graph = file_parser.GetPoseGraph();
    PRINTVERBOSE(verbose, "DONE\n");

    cv::Mat image(1000, 1000, CV_8UC3, cv::Scalar(255, 255, 255));
    g_t_graph.PrintGraph(image, 'b', 0);
    if (print_initial_guess)
      i_g_graph.PrintGraph(image, 'r', 2);
    cv::imshow("ground truth(blue), initial guess(red), optimized graph(green)", image);
    std::cout << "PRESS ANY KEY TO CONTINUE\n";
    cv::waitKey(0);
    PGOSolver optimizer(&i_g_graph, kernel_threshold, min_num_inliers, damping);
    char key = 0;
    while (key != OPENCV_KEY_ESCAPE) {
      optimizer.GoAndDoYourFunnyWork();
      if (verbose)
        optimizer.PrintStat();
      std::cout << "press ESC to stop and show the results or press SPACE to continue to optimize\n";
      key = cv::waitKey(0);
    }
    optimizer.PrintStat();
    i_g_graph.PrintGraph(image, 'g', 4);
    cv::imshow("ground truth(blue), initial guess(red), optimized graph(green)", image);
    std::cout << "press any key to exit\n";
    cv::waitKey(0);
    PRINTVERBOSE(verbose, "give me another graph!\n");
    return 0;
  } else {
    PrintHelp();
    exit(1);
  }
}
