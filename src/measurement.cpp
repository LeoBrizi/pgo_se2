#include <measurement.h>

Measurement::Measurement(unsigned int id_from, unsigned int id_to, float z_x, float z_y,
                        float z_theta, float omega_x_x, float omega_x_y, float omega_x_theta,
                        float omega_y_y, float omega_y_theta, float omega_theta_theta)
    : id_({id_from, id_to}) {
  this->observation_ = v2t(Eigen::Vector3f(z_x, z_y, z_theta));
  this->information_matrix_ << omega_x_x,     omega_x_y,      omega_x_theta,
                               omega_x_y,     omega_y_y,      omega_y_theta,
                               omega_x_theta, omega_y_theta,  omega_theta_theta;
}

Measurement::Measurement(unsigned int id_from, unsigned int id_to, const Eigen::Vector3f& observation,
                        const Eigen::Matrix3f& information_matrix)
    : id_({id_from, id_to}) {
  this->observation_ = v2t(observation);
  this->information_matrix_ = information_matrix;
}

Measurement::Measurement(unsigned int id_from, unsigned int id_to, const Eigen::Matrix3f& observation)
    : id_({id_from, id_to}) {
  this->observation_ = observation;
  this->information_matrix_.setIdentity();
}
