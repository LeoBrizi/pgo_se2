#include "pgo_solver.h"

PGOSolver::PGOSolver(PoseGraph *pose_graph, float kernel_threshold, int min_num_inliers, float damping) 
    : kernel_threshold_(kernel_threshold),
      min_num_inliers_(min_num_inliers),
      damping_(damping) {
  this->graph_ = pose_graph;
};

#ifndef SPARSE

  void AddBlockJacobian(Eigen::MatrixXd& H, const Link& link, const Matrix6_3f& jacobian_d_xj, float lambda) {
    Matrix6_3f jacobian_d_xi = -jacobian_d_xj;
    
    Eigen::Matrix3f block_i_i = jacobian_d_xi.transpose() * jacobian_d_xi * lambda;
    H.block<3,3>(link.from*3, link.from*3) += (block_i_i).cast<double>();
    
    Eigen::Matrix3f block_i_j = jacobian_d_xi.transpose() * jacobian_d_xj * lambda;
    H.block<3,3>(link.from*3, link.to*3) += (block_i_j).cast<double>();
    
    Eigen::Matrix3f block_j_i = jacobian_d_xj.transpose() * jacobian_d_xi * lambda;
    H.block<3,3>(link.to*3, link.from*3) += (block_j_i).cast<double>();
    
    Eigen::Matrix3f block_j_j = jacobian_d_xj.transpose() * jacobian_d_xj * lambda;
    H.block<3,3>(link.to*3, link.to*3) += (block_j_j).cast<double>();
    
    return;
  };

  void AddBlockB(Eigen::MatrixXd& b, const Link& link, const Vector6f& error,
                 const Matrix6_3f& jacobian_d_xj, float lambda) {
    
    Eigen::Vector3f block_i = (-jacobian_d_xj).transpose() * error * lambda;
    b.block<3,1>(link.from*3, 0) += (block_i).cast<double>();
    
    Eigen::Vector3f block_j = jacobian_d_xj.transpose() * error * lambda;
    b.block<3,1>(link.to*3, 0) += (block_j).cast<double>();
    
    return;
  };

#else
  
  void AddBlockJacobian(SparseMat& H, const Link& link, const Matrix6_3f& jacobian_d_xj, float lambda) {
    Matrix6_3f jacobian_d_xi = -jacobian_d_xj;
    #pragma omp sections
    {
      {
        Eigen::Matrix3f block_i_i = jacobian_d_xi.transpose() * jacobian_d_xi * lambda;
        for (int i = 0; i < 3; ++i)
          for (int j = 0; j < 3; ++j)
            H.coeffRef(i + link.from*3, j + link.from*3) += block_i_i(i, j);
      }
      #pragma omp section
      {
        Eigen::Matrix3f block_i_j = jacobian_d_xi.transpose() * jacobian_d_xj * lambda;
        for (int i = 0; i < 3; ++i)
          for (int j = 0; j < 3; ++j)
            H.coeffRef(i + link.from*3, j + link.to*3) += block_i_j(i, j);
      }
      #pragma omp section
      {
        Eigen::Matrix3f block_j_i = jacobian_d_xj.transpose() * jacobian_d_xi * lambda;
        for (int i = 0; i < 3; ++i)
          for (int j = 0; j < 3; ++j)
            H.coeffRef(i + link.to*3, j + link.from*3) += block_j_i(i, j);
      }
      #pragma omp section
      {
        Eigen::Matrix3f block_j_j = jacobian_d_xj.transpose() * jacobian_d_xj * lambda;
        for (int i = 0; i < 3; ++i)
          for (int j = 0; j < 3; ++j)
            H.coeffRef(i + link.to*3, j + link.to*3) += block_j_j(i, j);
      }
    }
    return;
  };

  void AddBlockB(SparseMat& b, const Link& link, const Vector6f& error,
                 const Matrix6_3f& jacobian_d_xj, float lambda) {
    #pragma omp sections
    {
      {
        Eigen::Vector3f block_i = (-1 * jacobian_d_xj).transpose() * error * lambda;
        for (int i = 0; i < 3; ++i)
          b.coeffRef(i + link.from*3, 0) += block_i(i);
      }
      #pragma omp section
      {
        Eigen::Vector3f block_j = jacobian_d_xj.transpose() * error * lambda;
        for (int j = 0; j < 3; ++j)
          b.coeffRef(j + link.to*3, 0) += block_j(j);
      }
    }
    return;
  };

#endif

bool PGOSolver::GoAndDoYourFunnyWork(bool ignore_outliers, bool print_h_mat) {
  int num_of_state = this->graph_->number_of_state_;

  #ifdef SPARSE

    SparseMat H(3*num_of_state, 3*num_of_state);
    SparseMat b(3*num_of_state, 1);
    H.reserve(Eigen::VectorXi::Constant(3*num_of_state, this->graph_->max_edge_one_node_));
  
  #else

    Eigen::MatrixXd H(3*num_of_state, 3*num_of_state);
    Eigen::MatrixXd b(3*num_of_state, 1);
    H.setZero();
    b.setZero();

  #endif
  
  this->num_inliers_ = 0;
  this->chi_inliers_ = 0;
  this->chi_outliers_ = 0;
  auto t_start_solve = std::chrono::system_clock::now();;
  
  for (auto &m : this->graph_->measurements_) {
    Link link = m.GetLink();
    Vector6f error;
    Matrix6_3f jacobian_d_xj;
    ErrorAndJacobian(m, error, jacobian_d_xj);

    float chi = error.dot(error);
    float lambda = 1;
    bool is_inlier = true;
    if (chi > this->kernel_threshold_) {
      lambda = sqrt(this->kernel_threshold_ / chi);
      is_inlier = false;
      this->chi_outliers_ += chi;
    } else {
      this->chi_inliers_ += chi;
      this->num_inliers_ ++;
    }
      
    if (!ignore_outliers || is_inlier) {
      AddBlockJacobian(H, link, jacobian_d_xj, lambda);
      AddBlockB(b, link, error, jacobian_d_xj, lambda);
    }
  }

  #ifdef SPARSE
    SparseMat damping_mat(3*num_of_state, 3*num_of_state);
    damping_mat.setIdentity();
    H += damping_mat * this->damping_;
    H.makeCompressed();
    Eigen::SimplicialCholesky<SparseMat> chol(H);
    Eigen::VectorXd delta_x = chol.solve(-b);
  
  #else

    H += Eigen::MatrixXd::Identity(3*num_of_state, 3*num_of_state) * this->damping_;
    Eigen::VectorXd delta_x = H.ldlt().solve(-b);

  #endif

  auto t_end_solve = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = t_end_solve - t_start_solve;

  this->optimization_time_ = elapsed_seconds.count();

  if (this->num_inliers_ < this->min_num_inliers_) {
    std::cerr << "too few inliers, skipping\n";
    return false;
  }

  #ifndef SPARSE

    if (print_h_mat) {
      cv::Mat H_img(3*num_of_state, 3*num_of_state, CV_8UC1, 255);
      for (int r = 0; r < H.rows(); r++)
        for(int c = 0; c < H.cols(); c++)
          if(H(r, c) != 0)
            H_img.at<char>(r, c) = 0;
      cv::imshow("H matrix", H_img);
    }

  #endif

  for (int i = 0; i < delta_x.size(); i += 3) {
    Eigen::Vector3f perturbation = (delta_x.block<3,1>(i, 0)).cast<float>();
    (this->graph_->states_.at(i/3)).BoxPlus(perturbation);
  }
  return true;
};

void PGOSolver::ErrorAndJacobian(const Measurement& measurement,
                                 Vector6f& error, Matrix6_3f& jacobian_d_xj) const {
  Link link = measurement.GetLink();
  const State& state_i = this->graph_->GetState(link.from);
  const State& state_j = this->graph_->GetState(link.to);
  Eigen::Matrix3f h_i_j = state_i.Predict(state_j);
  error = measurement.BoxMinus(h_i_j);

  Eigen::Matrix2f R_i, R_j, R_j_prime, D_h_D_theta;
  Eigen::Vector2f t_j;
  R_i = state_i.GetOrientation();
  R_j = state_j.GetOrientation();
  R_j_prime = RPrime(0);
  t_j = state_j.GetTranslation();
  D_h_D_theta = R_i.transpose() * R_j_prime * R_j;
  
  jacobian_d_xj.setZero();
  jacobian_d_xj.block<2,2>(4, 0) = R_i.transpose();
  jacobian_d_xj.block<2,1>(4, 2) = R_i.transpose() * R_j_prime * t_j;
  jacobian_d_xj.block<4,1>(0, 2) = Flatten(D_h_D_theta);
  
  return;
}

