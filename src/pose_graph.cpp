#include "pose_graph.h"

PoseGraph::PoseGraph(unsigned int first_id, unsigned int last_id, unsigned int max_edge_one_node)
    : first_id_(first_id),
      max_edge_one_node_(max_edge_one_node) {
  this->number_of_state_ = 0;
  this->map_id_vector_.resize(last_id - first_id +1, -1);
}

void PoseGraph::AddState(const State& state) {
  this->states_.push_back(state);
  this->map_id_vector_[state.GetID() - this->first_id_] = this->number_of_state_++; 
}

void PoseGraph::AddMeasurement(const Measurement& measurement) {
  this->measurements_.push_back(measurement);
}

void PoseGraph::PrintMeasurement(const cv::Mat& image, const Measurement& measurement,
                                cv::Point& img_center, int scaling, int line_thickness ) const {
  Link link = measurement.GetLink();
  const State& state_from = this->GetState(link.from);
  const State& state_to = this->GetState(link.to);
  cv::Point point_from = state_from.GetGraphicalRepresentation();
  cv::Point point_to = state_to.GetGraphicalRepresentation();

  point_from.y = - point_from.y;
  point_to.y = - point_to.y;
  point_from = point_from * scaling + img_center;
  point_to = point_to * scaling + img_center;
  line(image, point_from, point_to, cv::Scalar(255, 0, 0), line_thickness, CV_AA);
  return;
}

void PoseGraph::PrintGraph(const std::string &name_window, unsigned int img_dim,
                           int line_thickness, int state_radius ) const {
  cv::Mat image(img_dim, img_dim, CV_8UC3, cv::Scalar(255, 255, 255));
  cv::Point img_center(img_dim / 2, img_dim / 2);
  int scaling = 0;
  int max_coor = 0;
  std::vector<cv::Point> point_states;
  
  for (auto &s : this->states_) {
    cv::Point p_state = s.GetGraphicalRepresentation();
    point_states.push_back(p_state);
    if (std::abs(std::max(p_state.x, p_state.y)) > max_coor)
      max_coor = std::abs(std::max(p_state.x, p_state.y));
  }
  scaling = (img_dim/2 - 50) / max_coor;
  for (auto &s : point_states) {
    s.y = - s.y;
    cv::Point drawing_point = s * scaling + img_center;
    circle(image, drawing_point, state_radius, cv::Scalar(0, 0, 255), -1);
  }

  for (auto &m : this->measurements_)
    this->PrintMeasurement(image, m, img_center, scaling, line_thickness);
  
  cv::imshow(name_window, image);
  cv::waitKey(0);
  return;
}

void PoseGraph::PrintGraph(const cv::Mat& image, char color, int offset) const {
  cv::Point img_center(image.rows / 2, image.cols / 2);
  int scaling = 25;
  cv::Scalar state_color;
  cv::Point offset_vec(offset, offset);
  if (color == 'r')
    state_color = cv::Scalar(0, 0, 255);
  else if (color == 'g')
    state_color = cv::Scalar(0, 255, 0);
  else
    state_color = cv::Scalar(255, 0, 0);

  for (auto &s : this->states_) {
    cv::Point p_state = s.GetGraphicalRepresentation();
    p_state.y = - p_state.y;
    cv::Point drawing_point = p_state * scaling + img_center + offset_vec;
    circle(image, drawing_point, 5, state_color, -1);
  }
  return;
}