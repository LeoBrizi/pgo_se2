#include "state.h"

State::State(unsigned int id, float x, float y, float theta)
    : id_(id),
      theta_(theta) {
  Eigen::Vector3f pose(x, y , theta);
  this->pose_ = v2t(pose);
}

State::State(unsigned int id, const Eigen::Vector3f& pose)
    : id_(id),
      theta_(pose(2)) {
  this->pose_ = v2t(pose);
}

State::State(unsigned int id, const Eigen::Matrix3f& pose)
  : id_(id),
    pose_(pose) {
  Eigen::Vector3f vector_pose = t2v(pose);
  this->theta_ = vector_pose(2);
}

const cv::Point State::GetGraphicalRepresentation() const {
  cv::Point state_representation(this->pose_(0,2), this->pose_(1,2));
  return state_representation;
}

