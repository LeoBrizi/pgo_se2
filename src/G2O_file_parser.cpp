#include "G2O_file_parser.h"

G2OFileParser::G2OFileParser(const std::string &file_name) : file_name_(file_name) {
  std::ifstream g2o_file(file_name);
  std::string line;
  std::vector<int> id_conversion_vector;
  int num_of_states = 0;
  
  unsigned int max_edge_one_node = 0;
  unsigned int id_current_node = 0;
  unsigned int edge_current_node = 0;

  if (!g2o_file.is_open()) {
    std::cerr << "Unable to open file\n";
    exit(1);
  }

  while (std::getline(g2o_file, line)) {
    std::istringstream iss(line);
    std::string type;
    if (!(iss >> type)) { continue; }
    if (type.compare("VERTEX_SE2") == 0) {
      num_of_states ++;
    } else if (type.compare("EDGE_SE2") == 0) {
      unsigned int id;
      iss >> id;
      if (id_current_node == id)
        edge_current_node ++;
      else {
        id_current_node = id;
        edge_current_node = 0;
      }
      if (edge_current_node > max_edge_one_node)
        max_edge_one_node = edge_current_node;
    }
  }
  this->pose_graph_ = PoseGraph(0, num_of_states, max_edge_one_node);
  num_of_states = 0;

  g2o_file.clear();
  g2o_file.seekg(0, std::ios::beg);

  while (std::getline(g2o_file, line)) {
    std::istringstream iss(line);
    std::string type;
    if (!(iss >> type)) { continue; }
    if (type.compare("VERTEX_SE2") == 0) {
      unsigned int id;
      float x, y, theta;
      iss >> id >> x >> y >> theta;
      
      id_conversion_vector.push_back(id);
      State state(num_of_states, x, y, theta);
      num_of_states ++;
      this->pose_graph_.AddState(state);
    } else if (type.compare("EDGE_SE2") == 0) {
      unsigned int id_from, id_to;
      float z_x, z_y, z_theta, omega_x_x, omega_x_y, omega_x_theta, omega_y_y, omega_y_theta, omega_theta_theta;
      
      iss >> id_from >> id_to;
      iss >> z_x >> z_y >> z_theta;
      iss >> omega_x_x >> omega_x_y >> omega_x_theta >> omega_y_y >> omega_y_theta >> omega_theta_theta;

      std::vector<int>::iterator it = std::find(id_conversion_vector.begin(), id_conversion_vector.end(), id_from);
      int index_id_from = std::distance(id_conversion_vector.begin(), it);
      it = std::find(id_conversion_vector.begin(), id_conversion_vector.end(), id_to);
      int index_id_to = std::distance(id_conversion_vector.begin(), it);

      Measurement measurement(index_id_from, index_id_to, z_x, z_y, z_theta, omega_x_x, omega_x_y, omega_x_theta,
                              omega_y_y, omega_y_theta, omega_theta_theta);
      this->pose_graph_.AddMeasurement(measurement);
    } else
      continue;
  }
  g2o_file.close();
}

PoseGraph& G2OFileParser::GetPoseGraph() {
  return this->pose_graph_;
}