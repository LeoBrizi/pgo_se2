# pgo_se2

Project for probabilistic robotics course. It has the aim to develop a pose-pose graph slam system on SE(2)

### Dependencies
The following packages are required:

* build-essential
* cmake
* libeigen3-dev
* libopencv-dev

### Compilation
From the terminal:

* mkdir build
* cd build
* cmake ..
* make
    
### Execution
* cd build/bin
* ./show_graph -h
* ./pgo_se2_total -h

### Sparse version
Using "cmake .. -DUSE_SPARSE=ON" instead of "cmake .." in compilation phase


### TODO
* optimization at each measurement
* project information matrix in to manifold
	

For other information see this [link](./README.md).